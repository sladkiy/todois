import React, { useCallback, useState } from 'react';
import Badge from 'react-bootstrap/Badge';
import Button from 'react-bootstrap/Button';
import Toast from 'react-bootstrap/Toast';
import Form from 'react-bootstrap/Form';
import { useDispatch } from 'react-redux';
import { DateFormats } from '../../constants/dates';
import { format } from 'date-fns';
import { addNewTodo } from '../store/actions';

export const NewTodo: React.FC = () => {
    const put = useDispatch();
    const [description, setDescription] = useState('');
    const [dueDate, setDueDate] = useState<number>(Date.now());
    const [dueDateEnabled, setDueDateEnabled] = useState(true);

    const handleSubmit = useCallback((event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        put(addNewTodo({
            description,
            dueDate: dueDateEnabled ? dueDate : null,
        }))
        setDescription('');
        setDueDate(Date.now());
    }, [description, dueDate, dueDateEnabled, put]);

    const handleDescriptionChange = useCallback((event: React.ChangeEvent<HTMLTextAreaElement>) => {
        setDescription(event.target.value);
    }, []);

    const handleDueDateChange = useCallback((event: React.ChangeEvent<HTMLInputElement>) => {
        if (event.target.valueAsNumber) {
            setDueDate(event.target.valueAsNumber);
        };
    }, []);

    const handleDueDateSwitch = useCallback((event: React.ChangeEvent<HTMLInputElement>) => {
        setDueDateEnabled(event.target.checked)
    }, []);

    return (
        <Toast className="mw-100">
        <Toast.Header closeButton={ false }>
            <Badge className="mr-auto" variant="primary">New</Badge>
        </Toast.Header>
        <Toast.Body>
            <Form onSubmit={ handleSubmit }>
                <Form.Group>
                    <Form.Label>Description</Form.Label>
                    <Form.Control
                        size="sm"
                        as="textarea"
                        rows={ 2 }
                        placeholder="Enter description"
                        onChange={ handleDescriptionChange }
                        value={ description }
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label as="div" className="d-flex">
                        <span>Due date</span>
                        <Form.Check
                            className="ml-2"
                            checked={ dueDateEnabled }
                            onChange={ handleDueDateSwitch }
                        />
                    </Form.Label>
                    <Form.Control
                        size="sm"
                        type="date"
                        onChange={ handleDueDateChange }
                        value={ format(dueDate, DateFormats.DATE_INPUT) }
                        disabled={ !dueDateEnabled }
                    />
                </Form.Group>
                <div className="d-flex justify-content-end">
                    <Button
                        className="mr-2"
                        size="sm"
                        variant="outline-primary"
                        disabled={ !description }
                        type="submit"
                    >
                        Add
                    </Button>
                </div>
            </Form>
        </Toast.Body>
    </Toast>
    )
}