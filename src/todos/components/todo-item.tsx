import React, { useCallback, useMemo, useState } from 'react';
import Button from 'react-bootstrap/Button';
import Badge from 'react-bootstrap/Badge';
import Toast from 'react-bootstrap/Toast';
import { format, isBefore, startOfToday } from 'date-fns';
import { DateFormats } from '../../constants/dates';
import { useDispatch } from 'react-redux';
import { completeTodo, copyTodo, setEditingTodo } from '../store/actions';

enum Statuses {
    PAST_DUE = 'PAST_DUE',
    INCOMPLETED = 'INCOMPLETED',
    COMPLETED = 'COMPLETED',
}

const statusType = {
    [Statuses.PAST_DUE]: 'danger',
    [Statuses.INCOMPLETED]: 'light',
    [Statuses.COMPLETED]: 'success'
}

const statusText = {
    [Statuses.PAST_DUE]: 'Past due date',
    [Statuses.INCOMPLETED]: 'To do',
    [Statuses.COMPLETED]: 'Done'
}

type Props = {
    id: string;
    description: string;
    completed: boolean;
    dueDate: number | null;
    completeDate: number | null;
};

export const TodoItem: React.FC<Props> = ({
    description,
    dueDate,
    completed,
    id,
    completeDate,
}) => {
    const put = useDispatch();

    const status = useMemo(() => {
        if (completed) return Statuses.COMPLETED;

        if (dueDate && isBefore(dueDate, startOfToday())) return Statuses.PAST_DUE;

        return Statuses.INCOMPLETED;
    }, [completed, dueDate]);

    const handleCompleteClick = useCallback(() => {
        put(completeTodo(id));
    }, [id, put]);

    const handleTodoEdit = useCallback(() => {
        put(setEditingTodo(id));
    }, [id, put]);

    const handleTodoCopy = useCallback(() => {
        put(copyTodo(id));
    }, [id, put]);

    return (
        <Toast className="mw-100 h-100 d-flex flex-column">
            <Toast.Header closeButton={ false }>
                <Badge className="mr-auto" variant={ statusType[status] }>{ statusText[status] }</Badge>
                {
                    !completed && dueDate && (
                        <small className="mr-1">till: { format(dueDate, DateFormats.COMMON) }</small>
                        )
                }
                {
                    completed && completeDate && (
                            <small className="mr-1">completed: { format(completeDate, DateFormats.COMMON) }</small>
                    )
                }
            </Toast.Header>
            <Toast.Body className="d-flex flex-row align-items-stretch flex-grow-1">
                <div className="w-100 h-100 d-flex flex-column">
                    <div className="mb-2" style={ { textDecoration: completed ? 'line-through': '' } }>
                        { description }
                    </div>
                    <div className="mt-auto d-flex justify-content-end">
                        {
                            completed ? (
                                <Button
                                    size="sm"
                                    variant="outline-primary"
                                    onClick={ handleTodoCopy }
                                >
                                    Copy
                                </Button>
                            ) : (
                                <>
                                    <Button
                                        className="mr-2"
                                        size="sm"
                                        variant="outline-primary"
                                        onClick={ handleCompleteClick }
                                    >
                                        Complete
                                    </Button>
                                    <Button
                                        size="sm"
                                        variant="outline-primary"
                                        onClick={ handleTodoEdit }
                                    >
                                        Edit
                                    </Button>
                                </>
                            )
                        }
                    </div>
                </div>
            </Toast.Body>
        </Toast>
    )
};
