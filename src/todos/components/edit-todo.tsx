import React, { useCallback, useEffect } from 'react';
import Modal from 'react-bootstrap/Modal';
import { useDispatch, useSelector } from 'react-redux';
import Form from 'react-bootstrap/esm/Form';

import { selectAllTodos, selectEditingTodo } from '../store/selectors';
import { saveTodoChanges, setEditingTodo } from '../store/actions';
import Button from 'react-bootstrap/Button';
import { format } from 'date-fns';
import { DateFormats } from '../../constants/dates';
import { useState } from 'react';


export const EditTodo: React.FC = () => {
    const put = useDispatch();
    const editingTodo = useSelector(selectEditingTodo);
    const allTodos = useSelector(selectAllTodos);

    const [description, setDescription] = useState('');
    const [dueDate, setDueDate] = useState<number>(Date.now());
    const [dueDateEnabled, setDueDateEnabled] = useState(true);

    useEffect(() => {
        if (editingTodo && allTodos[editingTodo]) {
            const { dueDate } = allTodos[editingTodo];
            setDescription(allTodos[editingTodo].description);
            setDueDate(dueDate || Date.now());
            setDueDateEnabled(Boolean(dueDate));
        }
    }, [allTodos, editingTodo])

    const handleClose = useCallback(() => {
        put(setEditingTodo(null));
    }, [put]);

    const handleSubmit = useCallback((event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        if (editingTodo) {
            put(saveTodoChanges({
                description,
                dueDate: dueDateEnabled ? dueDate : null,
            }, editingTodo));
            put(setEditingTodo(null));
        }
    }, [description, dueDate, dueDateEnabled, editingTodo, put]);

    const handleDescriptionChange = useCallback((event: React.ChangeEvent<HTMLTextAreaElement>) => {
        setDescription(event.target.value);
    }, []);

    const handleDueDateChange = useCallback((event: React.ChangeEvent<HTMLInputElement>) => {
        if (event.target.valueAsNumber) {
            setDueDate(event.target.valueAsNumber);
        };
    }, []);

    const handleDueDateSwitch = useCallback((event: React.ChangeEvent<HTMLInputElement>) => {
        setDueDateEnabled(event.target.checked)
    }, []);

    return (
        <Modal show={ Boolean(editingTodo) } onHide={handleClose} centered>
            <Modal.Header closeButton>
                <Modal.Title>Editing</Modal.Title>
            </Modal.Header>
            <Form onSubmit={ handleSubmit }>
                <Modal.Body>
                    <Form.Group>
                        <Form.Label>Description</Form.Label>
                        <Form.Control
                            size="sm"
                            as="textarea"
                            rows={ 2 }
                            placeholder="Enter description"
                            onChange={ handleDescriptionChange }
                            value={ description }
                        />
                    </Form.Group>
                    <Form.Group>
                        <Form.Label as="div" className="d-flex">
                            <span>Due date</span>
                            <Form.Check
                                className="ml-2"
                                checked={ dueDateEnabled }
                                onChange={ handleDueDateSwitch }
                            />
                        </Form.Label>
                        <Form.Control
                            size="sm"
                            type="date"
                            onChange={ handleDueDateChange }
                            value={ format(dueDate || Date.now(), DateFormats.DATE_INPUT) }
                            disabled={ !dueDateEnabled }
                        />
                    </Form.Group>
                </Modal.Body>
                <Modal.Footer>
                    <Button size="sm" variant="outline-secondary" onClick={handleClose}>
                        Close
                    </Button>
                    <Button size="sm" variant="outline-primary" type="submit">
                        Save Changes
                    </Button>
                </Modal.Footer>
            </Form>
        </Modal>
    )
};
