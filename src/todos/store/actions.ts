import { TodosActionTypes } from "./action-types";

export type NewTodoDto = {
    description: string;
    dueDate: number | null;
}

export const completeTodo = (id: string) => ({
    type: TodosActionTypes.COMPLETE_TODO,
    id
})

export const addNewTodo = (todo: NewTodoDto) => ({
    type: TodosActionTypes.ADD_TODO,
    todo
})

export const setEditingTodo = (todo: string | null) => ({
    type: TodosActionTypes.SET_EDITING_TODO,
    todo
})

export const saveTodoChanges = (todo: NewTodoDto, id: string) => ({
    type: TodosActionTypes.SAVE_TODO_CHANGES,
    todo,
    id,
})

export const copyTodo = (id: string) => ({
    type: TodosActionTypes.COPY_TODO,
    id,
})