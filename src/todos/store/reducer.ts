import { v4 as uuidv4 } from 'uuid';

import * as actions from './actions'
import { TodosActionTypes } from './action-types';
import { InferValueTypes } from '../../types/common';

type ActionTypes = ReturnType<InferValueTypes<typeof actions>>;

export type TodoItem = {
    id: string;
    description: string;
    dueDate: number | null;
    completed: boolean;
    completeDate: number | null;
}

export type TodosState = {
    todoMap: Record<string, TodoItem>;
    editingTodo: string | null;
}

const initialState: TodosState = {
    editingTodo: null,
    todoMap: {},
}

export function todosReducer(state = initialState, action: ActionTypes): TodosState {
    switch (action.type) {
        case TodosActionTypes.COMPLETE_TODO:
            return {
                ...state,
                todoMap: {
                    ...state.todoMap,
                    [action.id]: {
                        ...state.todoMap[action.id],
                        completed: true,
                        completeDate: Date.now(),
                    }
                }
            }
        case TodosActionTypes.ADD_TODO: {
            const id = uuidv4();

            return {
                ...state,
                todoMap: {
                    ...state.todoMap,
                    [id]: {
                        ...action.todo,
                        id,
                        completed: false,
                        completeDate: null,
                    }
                }
            }
        }
        case TodosActionTypes.COPY_TODO: {
            const id = uuidv4();

            return {
                ...state,
                todoMap: {
                    ...state.todoMap,
                    [id]: {
                        ...state.todoMap[action.id],
                        completed: false,
                        dueDate: Date.now(),
                        completeDate: null,
                    }
                }
            }
        }
        case TodosActionTypes.SET_EDITING_TODO:
            return {
                ...state,
                editingTodo: action.todo,
            }
        case TodosActionTypes.SAVE_TODO_CHANGES:
            return {
                ...state,
                todoMap: {
                    ...state.todoMap,
                    [action.id]: {
                        ...state.todoMap[action.id],
                        ...action.todo,
                    }
                }
            }
        default:
            return state
    }
};
