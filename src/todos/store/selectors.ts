import isToday from "date-fns/isToday";
import { ApplicationState } from "../../store";
import { compareAsc, isBefore, startOfToday } from "date-fns";
import { TodoItem } from "./reducer";

const comparator = (a: TodoItem, b: TodoItem) => {
    if (!a.dueDate) return 1;
    if (!b.dueDate) return -1;

    return compareAsc(a.dueDate, b.dueDate)
}

export const selectIncompletedTodos = (state: ApplicationState) => Object.values(state.todos.todoMap)
    .filter((todo) => !todo.completed)
    .sort(comparator);

export const selectCompletedTodos = (state: ApplicationState) => Object.values(state.todos.todoMap)
    .filter((todo) => todo.completed)
    .sort(comparator);

export const selectOverdueTodos = (state: ApplicationState) => Object.values(state.todos.todoMap)
    .filter((todo) => {
        if (todo.completed) return false;
        if (!todo.dueDate) return true;

        if (isBefore(todo.dueDate, startOfToday()) || isToday(todo.dueDate)) return true;

        return false;
    })
    .sort(comparator)

export const selectEditingTodo = (state: ApplicationState) => state.todos.editingTodo;

export const selectAllTodos = (state: ApplicationState) => state.todos.todoMap;
