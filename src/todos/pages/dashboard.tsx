import React from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { useSelector } from 'react-redux';
import { TodoItem } from '../components/todo-item';
import { selectOverdueTodos, selectCompletedTodos } from '../store/selectors';
import { NewTodo } from '../components/new-todo';

export const Dashboard: React.FC = () => {
    const overduedTodos = useSelector(selectOverdueTodos);
    const completedTodos = useSelector(selectCompletedTodos);

    return (
        <div>
            {
                Boolean(overduedTodos.length) && (
                    <h4 className="mt-2 mb-0">Overdue or due today</h4>
                )
            }
            <Row className="pt-4 mt-n3" xs={ 1 } sm={ 2 } md={ 3 }>
                {
                    overduedTodos.map((todo) => (
                        <Col className="py-2" key={ todo.id }>
                            <TodoItem { ...todo } />
                        </Col>
                    ))
                }
            </Row>
            {
                Boolean(completedTodos.length) && (
                    <h4 className="mt-2 mb-0">Completed</h4>
                )
            }
            <Row className="pt-4 mt-n3" xs={ 1 } sm={ 2 } md={ 3 }>
                {
                    completedTodos.map((todo) => (
                        <Col className="py-2" key={ todo.id }>
                            <TodoItem { ...todo } />
                        </Col>
                    ))
                }
            </Row>
            <Row className="pt-4 mt-n3" xs={ 1 } sm={ 2 } md={ 3 }>
                <Col >
                    <NewTodo />
                </Col>
            </Row>
        </div>
    )
}
