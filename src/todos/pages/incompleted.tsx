import React from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { useSelector } from 'react-redux';
import { TodoItem } from '../components/todo-item';
import { selectIncompletedTodos } from '../store/selectors';
import { NewTodo } from '../components/new-todo';

export const IncompletedTasks: React.FC = () => {
    const incompletedTodos = useSelector(selectIncompletedTodos);

    return (
        <div>
            <Row className="pt-4" xs={ 1 } sm={ 2 } md={ 3 }>
                {
                    incompletedTodos.map((todo) => (
                        <Col className="py-2" key={ todo.id }>
                            <TodoItem { ...todo } />
                        </Col>
                    ))
                }
            </Row>
            <Row className="pt-4 mt-n3" xs={ 1 } sm={ 2 } md={ 3 }>
                <Col >
                    <NewTodo />
                </Col>
            </Row>
        </div>
    )
}
