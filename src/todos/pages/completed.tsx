import React from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { useSelector } from 'react-redux';
import { TodoItem } from '../components/todo-item';
import { selectCompletedTodos } from '../store/selectors';

export const CompletedTasks: React.FC = () => {
    const completedTodos = useSelector(selectCompletedTodos);

    return (
        <Row className="pt-4" xs={ 1 } sm={ 2 } md={ 3 }>
            {
                completedTodos.map((todo) => (
                    <Col className="py-2" key={ todo.id }>
                        <TodoItem
                            { ...todo }
                        />
                    </Col>
                ))
            }
        </Row>
    )
}
