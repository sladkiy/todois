import { compose } from 'redux';

export declare global {
    interface Window {
        __REDUX_DEVTOOLS_EXTENSION__: typeof compose;
    }
}
