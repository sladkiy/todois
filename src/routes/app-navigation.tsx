import React from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { NavLink } from 'react-router-dom';
import { Routes } from '../constants/routes';

export const AppNavigation: React.FC = () => (
    <Navbar bg="dark" variant="dark">
        <Nav>
            <Nav.Link
                as={ NavLink }
                to={ Routes.DASHBOARD }
                exact={ true }
                activeClassName="active"
            >
                Dashboard
            </Nav.Link>
            <Nav.Link
                as={ NavLink }
                to={ Routes.INCOMPLETED_TASKS }
                exact={ true }
                activeClassName="active"
            >
                Incompeted
            </Nav.Link>
            <Nav.Link
                as={ NavLink }
                to={ Routes.COMPLETED_TASKS }
                exact={ true }
                activeClassName="active"
            >
                Completed
            </Nav.Link>
        </Nav>
    </Navbar>
)