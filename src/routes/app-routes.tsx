import React from 'react';
import { Switch, Route } from 'react-router-dom';
import { Routes } from '../constants/routes';
import { CompletedTasks } from '../todos/pages/completed';
import { IncompletedTasks } from '../todos/pages/incompleted';
import { Dashboard } from '../todos/pages/dashboard';

export const AppRoutes: React.FC = () => (
    <Switch>
        <Route exact={ true } path={ Routes.DASHBOARD }>
            <Dashboard />
        </Route>
        <Route exact={ true } path={ Routes.INCOMPLETED_TASKS }>
            <IncompletedTasks />
        </Route>
        <Route exact={ true } path={ Routes.COMPLETED_TASKS }>
            <CompletedTasks />
        </Route>
    </Switch>
)
