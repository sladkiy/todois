import { combineReducers } from 'redux';
import { todosReducer, TodosState } from '../todos/store';

export type ApplicationState = Readonly<{
    todos: TodosState;
}>;

export const rootReducer = combineReducers<ApplicationState>({
    todos: todosReducer,
});