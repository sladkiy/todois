import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import Container from 'react-bootstrap/Container';

import { AppRoutes } from './routes/app-routes';
import { AppNavigation } from './routes/app-navigation';
import { EditTodo } from './todos/components/edit-todo';


import './app.css';


function App() {
  return (
    <Container fluid="lg" className="App">
      <Router>
        <AppNavigation />
        <AppRoutes />
      </Router>

      <EditTodo />
    </Container>
  );
}

export default App;
