export enum Routes {
    DASHBOARD = '/',
    INCOMPLETED_TASKS = '/incompleted',
    COMPLETED_TASKS = '/completed',
}