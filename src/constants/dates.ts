export enum DateFormats {
    COMMON = 'dd.MM.yyyy',
    DATE_INPUT = 'yyyy-MM-dd',
}